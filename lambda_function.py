import boto3
import collections
import datetime
import os
import time

ec = boto3.client('ec2')

# Main handler function
def lambda_handler(event, context):

  accountNumber = os.environ[$AWS_ACCOUNT_ID]
  retentionDays = int(os.environ[$RETENTION_DAYS])
  
  # Retriving all EC2 instances
  reservations = ec.describe_instances(
    Filters=[
      {'Name': 'tag:BackupNode', 'Values': ['true']}
    ]
  ).get(
    'Reservations', []
  )

  instances = sum(
    [
      [i for i in r['Instances']]
      for r in reservations
    ], [])

  #to_tag = collections.defaultdict(list)
  amiList = []
  # For each instance if they have Retention Tag, get the number of days from Instance tag else from lambda parameter
  for instance in instances:
    try:
      retention_days = [
        int(t.get('Value')) for t in instance['Tags']
        if t['Key'] == 'Retention'][0]
    except IndexError:
      retention_days = retentionDays

      create_time = datetime.datetime.now()
      create_fmt = create_time.strftime('%d-%m-%Y-%H-%M-%S')

      for tag in instance['Tags']:
        if tag['Key'] == 'Name':
          amiName = tag['Value']
          break

      nametag = amiName + "-" + create_fmt
      AMIid = ec.create_image(InstanceId=instance['InstanceId'], Name= nametag, Description="This is an AMI of " + instance['InstanceId'] + " on " + create_fmt, NoReboot=True, DryRun=False)

      delete_date = datetime.date.today() + datetime.timedelta(days=retention_days)
      delete_fmt = delete_date.strftime('%d-%m-%Y')

      ec.create_tags(
        Resources=[AMIid['ImageId']],
        Tags=[
          {'Key': 'DeleteOn', 'Value': delete_fmt},
          {'Key': 'BackupNode', 'Value': 'true'},
          {'Key': 'Name', 'Value': nametag}
        ]
      )

      # to_tag[retention_days].append(AMIid['ImageId'])

      amiList.append(AMIid['ImageId'])

  snapshotMaster = []
  time.sleep(10)

  for ami in amiList:
    snapshots = ec.describe_snapshots(
      DryRun=False,
      OwnerIds=[
        accountNumber
      ],
      Filters=[{
        'Name': 'description',
        'Values': [
          '*'+ami+'*'
        ]
      }]
    ).get(
      'Snapshots', []
    )

    delete_date = datetime.date.today() + datetime.timedelta(days=retention_days)
    delete_fmt = delete_date.strftime('%d-%m-%Y')
    snapnametag = ami + "-" + "Lambda-created"

    for snapshot in snapshots:

      ec.create_tags(
        Resources=[snapshot['SnapshotId']],
        Tags=[
          {'Key': 'DeleteOn', 'Value': delete_fmt},
          {'Key': 'Backup', 'Value': 'True'},
          {'Key': 'Name', 'Value': snapnametag},
        ]
      )
